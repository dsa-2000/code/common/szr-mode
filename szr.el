;; szr.el --- Major mode for Structurizr DSL
;;
;; Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
;; SPDX-License-Identifier: MIT

;;; Commentary:

;;

;;; Code:
(require 'simple-indentation)

(defvar szr-mode-hook nil)

;; (defvar szr-mode-map
;;   (let ((map (make-sparse-keymap)))
;;     map)
;;   "Keymap for `szr-mode'.")

(defvar szr-mode-syntax-table
  (let ((st (make-syntax-table)))
    (modify-syntax-entry ?# "<" st)
    (modify-syntax-entry ?/ ". 124" st)
    (modify-syntax-entry ?* ". 23b" st)
    (modify-syntax-entry ?\n ">" st)
    st)
  "Syntax table for `szr-mode'.")

;;;###autoload
;(add-to-list 'auto-mode-alist '("\\.dsl\\'" . szr-mode))

(defvar szr-font-lock-keywords
  (let ((kw (regexp-opt
             '("workspace" "model" "group" "person" "softwareSystem" "container"
               "component" "deploymentEnvironment" "deploymentGroup" "deploymentNode"
               "infrastructureNode" "softwareSystemInstance" "containerInstance"
               "healthCheck" "element" "relationship" "tags" "description"
               "technology" "instances" "url" "properties" "perspectives" "!ref"
               "views" "systemLandscape" "systemContext" "filtered" "dynamic"
               "image" "include" "exclude" "autoLayout" "default" "animation" "title"
               "styles" "theme" "themes" "branding" "terminology" "configuration"
               "users" "extends" "shape") t))
        (iddef "\\<\\(\\(\\sw\\|\\.\\)+\\)\\s-*\\(=\\|->\\)")
        (iduse "\\(->\\|systemContext\\|container\\)\\s-*\\(\\(\\sw\\|\\.\\)+\\)\\>")
        (bi-id "\\(!identifiers\\s-+\\(flat\\|hierarchical\\)\\>\\)")
        (bi-ir "\\(!impliedRelationships\\s-+\\(true\\|false\\)\\>\\)")
        (bi-sc "\\(!script\\s-+\\(groovy\\|kotlin\\|javascript\\|ruby\\)\\)")
        (bi-shp
         (concat
          "\\(shape\\s-+"
          (regexp-opt
           '("Box" "RoundedBox" "Circle" "Ellipse" "Hexagon" "Cylinder"
             "Pipe" "Person" "Robot" "Folder" "WebBrowser"
             "MobileDevicePortrait" "MobileDeviceLandscape" "Component") t)
          "\\)"))
        (ppr (regexp-opt
              '("!constant" "!docs" "!adrs" "!identifiers" "!impliedRelationships"
                "!include" "!plugin" "!script"))))
    (list
     (list ppr '(0 font-lock-preprocessor-face))
     (list bi-id '(2 font-lock-builtin-face))
     (list bi-ir '(2 font-lock-builtin-face))
     (list bi-sc '(2 font-lock-builtin-face))
     (list bi-shp '(2 font-lock-builtin-face))
     (list kw '(0 font-lock-keyword-face))
     (list iddef '(1 font-lock-warning-face))
     (list iduse '(2 font-lock-warning-face))))
  "Keyword highlighting specification for `szr-mode'.")

;; (defvar szr-imenu-generic-expression
;;   ...)

(defvar szr-outline-regexp ".*{$")

 ;;;###autoload
(define-derived-mode szr-mode prog-mode "Szr"
  "A major mode for editing Structurizr DSL files."
  :syntax-table szr-mode-syntax-table
  (setq-local comment-start "# ")
  (setq-local comment-start-skip "#+\\s-*")
  (setq-local font-lock-defaults
	            '(szr-font-lock-keywords nil t))
  (setq-local indent-line-function 'szr-indent-line)
  (setq-local indent-region-function 'szr-indent-region)
  ;; (setq-local imenu-generic-expression
	;;             szr-imenu-generic-expression)
  (setq-local outline-regexp szr-outline-regexp))

 ;;; Indentation

(simple-indentation-define-for-major-mode
 szr szr
 :rules
 (list
  (simple-indentation-rules-make
   :begin
   :on-chars-in-code "}"
   :and
   :on-chars-in-code "{"
   :end
   :on-current-or-previous-code-line)
  (simple-indentation-rules-make
   :add-indent
   :on-chars-in-code "{"
   :check-on-prev-code-line)
  (simple-indentation-rules-make
   :deindent
   :on-chars-in-code "}")))

(provide 'szr)
 ;;; szr.el ends here
