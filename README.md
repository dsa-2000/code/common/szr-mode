# szr-mode

Emacs mode for Structurizr DSL files. Very much a work in progress,
but basic syntax highlighting and indentation are
implemented. Indentation depends on the package `simple-indentation`
(available on MELPA), which doesn't seem ideal, but is a reasonable
start (the standard package SMIE would be preferable, but I had some
trouble using it here).
